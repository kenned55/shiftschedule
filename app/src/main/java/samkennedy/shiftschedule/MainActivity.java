package samkennedy.shiftschedule;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.EditText;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    public static final String KEY_DATE = "keyDate";
    public static final String KEY_START = "keyStartTime";
    public static final String KEY_FINISH = "keyFinishTime";
    public static final String KEY_NOTE = "keyNote";
    private static final int REQUEST_EDIT = 123;

    private static final String TAG = "ShiftActivity";
    private EditText editTextStartTime;
    private EditText editTextFinishTime;
    private EditText editTextshiftDate;
    private EditText editTextNote;
    private TextView textViewBlock;

    private Shifts shifts;
    private ArrayList<Shifts> shiftList = new ArrayList<>();
    private StringBuilder outputs;
    private static Double depreciation;

    public void onButtonClick(View view) {

        String strStartTime = editTextStartTime.getText().toString();
        String strFinishTime = editTextFinishTime.getText().toString();
        String strShiftDate = editTextshiftDate.getText().toString();

        int intStartTime = Integer.parseInt(strStartTime);
        int intFinishTime = Integer.parseInt(strFinishTime);



        if (strShiftDate.matches("")) {     //checks if shiftDate is empty
            shifts = new Shifts(intStartTime, intFinishTime);
        }
        if (strStartTime.matches("")) {     //checks if startTime is empty
            shifts = new Shifts(intFinishTime, strShiftDate);
        }
        if (strFinishTime.matches("")) {   //checks if finishTime is empty
            shifts = new Shifts(intStartTime, strShiftDate);
        } else {
            shifts = new Shifts(intStartTime, intFinishTime, strShiftDate);
        }
        Toast.makeText(getApplicationContext(), shifts.getMessage(), Toast.LENGTH_SHORT).show();
        Log.d(TAG, "User clicked " + Shifts.counter + " times.");
        Log.d(TAG, "User message is \"" + shifts + "\".");
    }

    public void goEdit(View v ) {
        Intent intentEdit = new Intent(this, NoteEditingActivity.class);
        startActivityForResult(intentEdit, REQUEST_EDIT);
    }
    public void goDisplay(View v) {
        Intent intentDisplay = new Intent();

        intentDisplay.setAction("com.example.kenned55.myactivities.ThirdActivity");
            intentDisplay.putExtra(KEY_DATE, editTextshiftDate.getText().toString());
            intentDisplay.putExtra(KEY_START, Integer.parseInt(editTextStartTime.getText().toString()));
            intentDisplay.putExtra(KEY_FINISH, Integer.parseInt(editTextFinishTime.getText().toString()));
            Bundle bundle = new Bundle();
            bundle.putString(KEY_NOTE, editTextNote.getText().toString());
            intentDisplay.putExtras(bundle);
            startActivity(intentDisplay);
    }

    private void resetOutputs() {
        if (shiftList.size() == 0) {
            outputs = new StringBuilder("No Shifts in the list.");
        } else {
            outputs = new StringBuilder();
            for (Shifts s : shiftList) {
                outputs.append("This is Shift No. " + shiftList.indexOf(s) +
                        System.getProperty("line.separator"));
                outputs.append("Shift is on the date: " + s.getDate() +
                        System.getProperty("line.separator"));
                outputs.append("Starting time : " + s.getStartTime() +
                        System.getProperty("line.separator"));
                outputs.append("Finishing time : " + s.getEndTime() +
                        System.getProperty("line.separator"));
            }
        }
        textViewBlock.setText(outputs);
    }

    public String getTest(){
        return "test Get";
    }


    private void addShift() {
        shiftList.add(shifts);
        resetOutputs();
    }

    private boolean clearShiftList() {
        shiftList.clear();
        resetOutputs();
        return true;
    }

    public void onClearClick(View v){
        clearShiftList();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);

        editTextStartTime = (EditText) findViewById(R.id.inputStartTime);
        editTextFinishTime = (EditText) findViewById(R.id.intputEndTime);
        editTextshiftDate = (EditText) findViewById(R.id.inputDate);
        editTextNote = (EditText) findViewById(R.id.inputNote);
        textViewBlock = (TextView) findViewById(R.id.block);
        textViewBlock.setMovementMethod(new ScrollingMovementMethod());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_EDIT && resultCode == RESULT_OK) {
            editTextNote.setText(data.getData().toString());
        }
        super.onActivityResult(requestCode, resultCode, data);
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.menu_add:
                addShift();
                return true;
            case R.id.menu_clear:
                return clearShiftList();
            case R.id.action_setting:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}