package samkennedy.shiftschedule;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class NoteEditingActivity extends AppCompatActivity {

    EditText editText;
    private TextView textview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_editing);
        editText = (EditText) findViewById(R.id.inputNote);
        textview = (TextView) findViewById(R.id.infoDisplay);

        StringBuilder mainMessage = new StringBuilder();
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();     //cound cause errors check intent and key vales in MainActivity
        mainMessage.append("Date: " + intent.getStringExtra(MainActivity.KEY_DATE) +
                System.getProperty("line.separator"));
        mainMessage.append("Start time: " + intent.getIntExtra(MainActivity.KEY_START, 0) +
                System.getProperty("line.separator"));
        mainMessage.append("Finish time: " + intent.getIntExtra(MainActivity.KEY_FINISH, 0) +
                System.getProperty("line.separator"));
        //mainMessage.append("Note : "+  bundle.getString(MainActivity.KEY_NOTE) +
        //       System.getProperty("line.separator"));                                     //need to add note box to mainPage
        //textview.setText(mainMessage);

    }

    public void onDoneClick(View v) {
       // Intent intent = new Intent();
        //Uri uri = Uri.parse(editText.getText().toString());
        //intent.setData(uri);
        //setResult(RESULT_OK, intent);

        finish();
    }
}
