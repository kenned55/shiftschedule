package samkennedy.shiftschedule;

/**
 * Created by samke on 27/03/2017.
 */

public class Shifts {
    public static int counter = 0;
    private int startTime;
    private int finishTime;
    private String shiftDate;
    private String message;

    //main constructor
    public Shifts() {
        this.startTime = 1900;
        this.finishTime = 2330;
        this.shiftDate = "29/03/2017";
        this.message = "This is the default message.";
    }

    //takes all declared variables, and produces shift
    public Shifts(int startTime, int finishTime, String shiftDate) {
        this.startTime = startTime;
        this.finishTime = finishTime;
        this.shiftDate = shiftDate;
        this.message = "Your shift starts at " + startTime + " and finishs at " +
                finishTime + "." + "On the " + shiftDate;
        count();
    }
    //Error - no finishTime input
    public Shifts(String shiftDate, int startTime) {
        this.startTime = startTime;
        this.shiftDate = shiftDate;
        this.message = "Please enter a finish time";
        count();
    }

    //Error - no startTime input
    public Shifts(int finishTime, String shiftDate) {
        this.finishTime = finishTime;
        this.shiftDate = shiftDate;
        this.message = "Please enter a start time";
        count();
    }

    //Error - no shiftDate input
    public Shifts(int startTime, int finishTime) {
        this.startTime = startTime;
        this.finishTime = finishTime;
        this.message = "Please enter a shift date";
        count();
    }

    public void count() {
        this.counter++;
    }

    @Override
    public String toString() {
        return super.toString();
    }

    public String getMessage() {
        return message;
    }

    public String getDate() {
        return shiftDate;
    }

    public int getStartTime() {
        return startTime;
    }

    public int getEndTime() {
        return finishTime;
    }
}
